/**
 * @author richt / http://richt.me
 * @author WestLangley / http://github.com/WestLangley
 *
 * W3C Device Orientation control (http://w3c.github.io/deviceorientation/spec-source-orientation.html)
 */

THREE.DeviceOrientationControls_a5 = function ( object ) {

	var scope = this;

	this.object = object;
	this.object.rotation.reorder( "YXZ" );

	this.enabled = true;

	this.deviceOrientation = {};
	this.screenOrientation = 0;

    this.oldA=0;
    this.oldB=0;
    this.oldG=0;
    this.filter = 5;
    this.fA_min = this.filter;
    this.fA_max = 60;
    this.fB_min = this.filter;
    this.fB_max = 60;
    this.fG_min = this.filter;
    this.fG_max = 60;
    this.initValue = false;
    this.steps = 10;
    this.interval = 10;


	var onDeviceOrientationChangeEvent = function ( event ) {

		scope.deviceOrientation = event;

	};

	var onScreenOrientationChangeEvent = function () {

		scope.screenOrientation = window.orientation || 0;

	};

	// The angles alpha, beta and gamma form a set of intrinsic Tait-Bryan angles of type Z-X'-Y''

	var setObjectQuaternion = function () {

		var zee = new THREE.Vector3( 0, 0, 1 );

		var euler = new THREE.Euler();

		var q0 = new THREE.Quaternion();

		var q1 = new THREE.Quaternion( - Math.sqrt( 0.5 ), 0, 0, Math.sqrt( 0.5 ) ); // - PI/2 around the x-axis

		return function ( quaternion, alpha, beta, gamma, orient ) {

			euler.set( beta, alpha, - gamma, 'YXZ' );                       // 'ZXY' for the device, but 'YXZ' for us

			quaternion.setFromEuler( euler );                               // orient the device

			quaternion.multiply( q1 );                                      // camera looks out the back of the device, not the top

			quaternion.multiply( q0.setFromAxisAngle( zee, - orient ) );    // adjust for screen orientation

		}

	}();

	this.connect = function() {

		onScreenOrientationChangeEvent(); // run once on load

		window.addEventListener( 'orientationchange', onScreenOrientationChangeEvent, false );
		window.addEventListener( 'deviceorientation', onDeviceOrientationChangeEvent, false );

		scope.enabled = true;

	};

	this.disconnect = function() {

		window.removeEventListener( 'orientationchange', onScreenOrientationChangeEvent, false );
		window.removeEventListener( 'deviceorientation', onDeviceOrientationChangeEvent, false );

		scope.enabled = false;

	};

	this.update = function () {

        if (scope.enabled === false) return;

        var e = scope.deviceOrientation;

        if (!this.initValue) {
            this.oldA = scope.deviceOrientation.alpha ? e.alpha : 0;
            this.oldB = scope.deviceOrientation.beta ? e.beta : 0;
            this.oldG = scope.deviceOrientation.gamma ? e.gamma : 0;
            this.initValue = true;
            return;
        }


        if (scope.deviceOrientation.alpha) {
        	var dA = Math.abs(Math.abs(e.alpha+360) - Math.abs(this.oldA+360));

            if (dA > this.fA_min){
                //this.oldA = e.alpha;
              	this.oldA = (dA > this.fA_max)?e.alpha : this.oldA + (e.alpha-this.oldA)/this.steps;
            }
        }
      
        if (scope.deviceOrientation.beta) {
        	var dB = Math.abs(Math.abs(e.beta+360)-Math.abs(this.oldB+360));
        	this.oldB = (dB < this.fB_min || dB > this.fB_max) ? this.oldB : e.beta;

            if (dB > this.fB_max){
                //this.oldA = e.beta;
              	this.oldB = (dB > this.fB_max)?e.beta : this.oldB + (e.beta-this.oldB)/this.steps;
            }
        }
      
        if (scope.deviceOrientation.gamma) {
        	var dG = Math.abs(Math.abs(e.gamma+360)-Math.abs(this.oldG+360));
        	this.oldG = (dG < this.fG_min || dG > this.fG_max) ? this.oldG : e.gamma;
          
            if (dG > this.fG_max){
                //this.oldG = e.gamma;
              	this.oldG = (dG > this.fG_max)?e.gamma : this.oldG + (e.gamma-this.oldG)/this.steps;
            }
        }

		var alpha  = THREE.Math.degToRad( this.oldA ); // Z
		var beta   = THREE.Math.degToRad( this.oldB ); // X'
		var gamma  = THREE.Math.degToRad( this.oldG ); // Y''
		var orient = scope.screenOrientation       ? THREE.Math.degToRad( scope.screenOrientation       ) : 0; // O
      
        //document.getElementById('data').innerHTML = 'a='+this.oldA+', b='+this.oldB+', g='+this.oldG;
        //document.getElementById('data').innerHTML = 'dA='+Math.round(dA*100)/100+', dB='+Math.round(dB*100)/100+', dG='+Math.round(dG*100)/100;

		setObjectQuaternion( scope.object.quaternion, alpha, beta, gamma, orient );

	};

	this.dispose = function () {

		this.disconnect();

	};

	this.connect();

};
