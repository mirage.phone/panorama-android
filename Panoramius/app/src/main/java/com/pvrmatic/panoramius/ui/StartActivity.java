package com.pvrmatic.panoramius.ui;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.pvrmatic.panoramius.R;

public class StartActivity extends AppCompatActivity {

    private boolean stop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        // wait for a second to show splash screen
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!stop) {
                    Intent startScanActivity = new Intent(StartActivity.this, ScanActivity.class);
                    startActivity(startScanActivity);

                    finish();
                }
            }
        }, 1000);
    }

    @Override
    public void onBackPressed() {
        stop = true;
        super.onBackPressed();
    }
}
