package com.pvrmatic.panoramius.ui;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import com.pvrmatic.panoramius.utils.Metrics;
import com.pvrmatic.panoramius.R;

public class HelpActivity extends AppCompatActivity {
    enum ClickArea {
        Back, Next, Start, None
    }

    private static class PagerTouchListener implements View.OnTouchListener {



        private float downX;
        private float downY;
        private HelpActivity helpActivity;

        public PagerTouchListener(HelpActivity helpActivity) {
            this.helpActivity = helpActivity;
        }

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {

            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    downX = motionEvent.getX();
                    downY = motionEvent.getY();
                    break;
                case MotionEvent.ACTION_UP:
                    float distance = calcDistance(motionEvent);
                    // do click
                    if (distance < 20) {
                        return doClick(motionEvent);
                    }
                    break;
            }
            return false;
        }

        private float calcDistance(MotionEvent upEvent) {
            float dx = upEvent.getX() - downX;
            float dy = upEvent.getY() - downY;
            return (float) Math.sqrt(dx * dx + dy * dy);
        }

        private boolean doClick(MotionEvent clickEvent) {
            ClickArea area = calcClickArea(clickEvent);
            helpActivity.onClick(area);
            return area != ClickArea.None;
        }

        private ClickArea calcClickArea(MotionEvent clickEven) {

            float xdp = Metrics.pxToDp(clickEven.getX());
            float ydp = Metrics.pxToDp(clickEven.getY());

            float widthdp = Metrics.pxToDp(Metrics.widthPx());
            float heightdp = Metrics.pxToDp(Metrics.heightPx());

            if (0 <= xdp && xdp <= 75 && 0 <= ydp && ydp <= 75) {
                return ClickArea.Back;
            }

            if (widthdp - 75 <= xdp && xdp <= widthdp && 0 <= ydp && ydp <= 75) {
                return ClickArea.Next;
            }

            if (widthdp / 2 - 100 <= xdp && xdp <= widthdp / 2 + 100
                    && heightdp / 2 - 100 <= ydp && ydp <= heightdp / 2 + 100) {
                return ClickArea.Start;
            }

            return ClickArea.None;
        }
    }

    private final class HelpPagerAdapter extends FragmentStatePagerAdapter {

        private HelpPageFragment[] pages;
        private static final int pageCount = 6;
        public HelpPagerAdapter(FragmentManager fm) {
            super(fm);

            pages = new HelpPageFragment[pageCount];
        }

        @Override
        public Fragment getItem(int position) {
            HelpPageFragment fragment = pages[position];
            if (fragment == null) {
                fragment = new HelpPageFragment();

                final Bundle arguments = new Bundle();
                arguments.putInt(HelpPageFragment.INDEX_KEY, position);
                fragment.setArguments(arguments);

                pages[position] = fragment;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return pageCount;
        }
    }

    ViewPager helpPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        helpPager = (ViewPager) findViewById(R.id.helpPager);
        helpPager.setAdapter(new HelpPagerAdapter(getSupportFragmentManager()));
        helpPager.setOnTouchListener(new PagerTouchListener(this));
    }

    public void onClick(ClickArea area) {

        switch (area) {
            case Back:
                if (helpPager.getCurrentItem() > 0) {
                    helpPager.setCurrentItem(helpPager.getCurrentItem() - 1, true);
                }
                break;
            case Next:
                if (helpPager.getCurrentItem() < 5) {
                    helpPager.setCurrentItem(helpPager.getCurrentItem() + 1, true);
                }
                break;
            case Start:
                if (helpPager.getCurrentItem() == 5) {
                    finish();
                    WebActivityUtils.startWebViewActivity(this);
                }

                break;

        }
    }
}


