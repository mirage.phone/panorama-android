package com.pvrmatic.panoramius;

import android.app.Activity;

import com.pvrmatic.panoramius.utils.Trace;

import java.io.InputStream;

import fi.iki.elonen.NanoHTTPD;

/**
 * Created by mgukov on 13.09.2016.
 */
public final class LocalFileServer extends NanoHTTPD {
    public static final int PORT = 20000;
    public static final String HOST_NAME = "localhost";
    public static final String PATH = "http://" + HOST_NAME + ":" + PORT + "/";

    private final Activity activity;

    public LocalFileServer(Activity activity) {
        super(HOST_NAME, PORT);
        this.activity = activity;
    }

    @Override
    public Response serve(IHTTPSession session) {
        InputStream inputStream = null;
        try {
            Trace.debug("Loading file: " + session.getUri());
            inputStream = activity.getAssets().open(session.getUri().substring(1));
        } catch (Exception e) {
            Trace.error("Error on load file", e);
        }

        return NanoHTTPD.newChunkedResponse(Response.Status.OK, MIME_HTML, inputStream);
    }
}
