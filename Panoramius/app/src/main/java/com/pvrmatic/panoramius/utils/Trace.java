package com.pvrmatic.panoramius.utils;

import android.util.Log;

/**
 * Created by mgukov on 08.09.2016.
 */
public final class Trace {

    public static final String DEBUG = "DEBUG";
    public static final String ERROR = "ERROR";

    public static void debug(String msg) {
        Log.d(DEBUG, msg);
    }

    public static void error(String msg, Throwable err) {
        Log.e(ERROR, msg, err);
    }

    public static void error(String msg) {
        Log.e(ERROR, msg);
    }

    private Trace() {}
}
