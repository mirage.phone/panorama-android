package com.pvrmatic.panoramius.ui;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.util.Log;

import com.pvrmatic.panoramius.ApplicationSession;
import com.pvrmatic.panoramius.utils.Trace;
import com.vuforia.Device;
import com.vuforia.State;
import com.vuforia.Trackable;
import com.vuforia.TrackableResult;
import com.vuforia.Vuforia;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;


// The renderer class for the ImageTargets sample. 
public class CameraRenderer implements GLSurfaceView.Renderer {

    private ApplicationSession vuforiaAppSession;
    private ScanActivity mActivity;
    private ScanRenderer mScanRenderer;

    boolean mIsActive = false;

    public CameraRenderer(ScanActivity activity, ApplicationSession session) {
        mActivity = activity;
        vuforiaAppSession = session;

        // ScanRenderer used to encapsulate the use of RenderingPrimitives setting
        // the device mode AR/VR and stereo mode
        mScanRenderer = new ScanRenderer(this, Device.MODE.MODE_AR, false);
    }


    // Called to draw the current frame.
    @Override
    public void onDrawFrame(GL10 gl) {
        if (!mIsActive)
            return;

        // Call our function to render content from ScanRenderer class
        mScanRenderer.render();
    }


    public void setActive(boolean active) {
        mIsActive = active;
    }


    // Called when the surface is created or recreated.
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        Trace.debug("GLRenderer.onSurfaceCreated");

        // Call Vuforia function to (re)initialize rendering after first use
        // or after OpenGL ES context was lost (e.g. after onPause/onResume):
        vuforiaAppSession.onSurfaceCreated();
        mScanRenderer.onSurfaceCreated();
    }


    // Called when the surface changed size.
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        Trace.debug("GLRenderer.onSurfaceChanged");

        // Call Vuforia function to handle render surface size changes:
        vuforiaAppSession.onSurfaceChanged(width, height);

        // RenderingPrimitives to be updated when some rendering change is done
        mScanRenderer.onConfigurationChanged();

        initRendering();
    }

    // Function for initializing the renderer.
    private void initRendering() {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, Vuforia.requiresAlpha() ? 0.0f : 1.0f);
    }

    public void updateConfiguration() {
        mScanRenderer.onConfigurationChanged();
    }

    // The render function called from SampleAppRendering by using RenderingPrimitives views.
    // The state is owned by ScanRenderer which is controlling it's lifecycle.
    // State should not be cached outside this method.
    public void renderFrame(State state, float[] projectionMatrix) {
        // Renders video background replacing Renderer.DrawVideoBackground()
        mScanRenderer.renderVideoBackground();
        // Did we find any trackables this frame?
        for (int tIdx = 0; tIdx < state.getNumTrackableResults(); tIdx++) {
            TrackableResult result = state.getTrackableResult(tIdx);
            Trackable trackable = result.getTrackable();

            onRecognizeImage(trackable);
        }
    }

    private void onRecognizeImage(Trackable trackable) {

        if (!mActivity.started) {
            return;
        }

        String userData = (String) trackable.getUserData();

        Trace.debug("----------------> Find image:	\"" + userData + "\"");

        mActivity.started = false;
        mActivity.startWebActivity();
    }
}
