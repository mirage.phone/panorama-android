package com.pvrmatic.panoramius.ui;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ToggleButton;

import com.pvrmatic.panoramius.R;
import com.pvrmatic.panoramius.ApplicationControl;
import com.pvrmatic.panoramius.VuforiaApplicationException;
import com.pvrmatic.panoramius.ApplicationSession;
import com.vuforia.CameraDevice;
import com.vuforia.DataSet;
import com.vuforia.ObjectTracker;
import com.vuforia.STORAGE_TYPE;
import com.vuforia.State;
import com.vuforia.Trackable;
import com.vuforia.Tracker;
import com.vuforia.TrackerManager;
import com.vuforia.Vuforia;

public class ScanActivity extends AppCompatActivity implements ApplicationControl {

    private ApplicationSession vuforiaAppSession;
    private DataSet dataset;

    private String targetFileName = "Kosmos.xml";
    private GLSurfaceView mGlView;

    CameraRenderer mRenderer;

    public boolean started;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        vuforiaAppSession = new ApplicationSession(this);
        vuforiaAppSession.initAR(this, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        initControl();
    }

    private void initControl() {
        final Button helpButton = (Button) findViewById(R.id.helpBtn);
        helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startHelpActivity();
            }
        });

        final ToggleButton switchBtn = (ToggleButton) findViewById(R.id.switchBtn);
        switchBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                WebActivityUtils.setViewMode(b ? WebActivityUtils.ViewMode3D : WebActivityUtils.ViewMode2D);
            }
        });
    }

    private void startHelpActivity() {
        final Intent startHelpIntent = new Intent(this, HelpActivity.class);
        startActivity(startHelpIntent);
    }

    public void startWebActivity() {
        WebActivityUtils.startWebViewActivity(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            vuforiaAppSession.resumeAR();
        } catch (VuforiaApplicationException e) {
            Log.e("Log", e.getString());
        }

        started = true;
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            vuforiaAppSession.pauseAR();
        } catch (VuforiaApplicationException e) {
            Log.e("Log", e.getString());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            vuforiaAppSession.stopAR();
        } catch (VuforiaApplicationException e) {
            Log.e("Log", e.getString());
        }
        System.gc();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        vuforiaAppSession.onConfigurationChanged();

        super.onConfigurationChanged(newConfig);
    }


    @Override
    public boolean doInitTrackers() {
        // Indicate if the trackers were initialized correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        Tracker tracker;

        // Trying to initialize the image tracker
        tracker = tManager.initTracker(ObjectTracker.getClassType());
        if (tracker == null) {
            Log.e("Log",
                    "Tracker not initialized. Tracker already initialized or the camera is already started");
            result = false;
        } else {
            Log.i("Log", "Tracker successfully initialized");
        }
        return result;
    }

    @Override
    public boolean doLoadTrackersData() {
        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        if (dataset == null)
            dataset = objectTracker.createDataSet();

        if (dataset == null)
            return false;

        if (!dataset.load(targetFileName, STORAGE_TYPE.STORAGE_APPRESOURCE)) {
            return false;
        }

        if (!objectTracker.activateDataSet(dataset))
            return false;

        int numTrackables = dataset.getNumTrackables();
        for (int count = 0; count < numTrackables; count++) {
            Trackable trackable = dataset.getTrackable(count);
            String name = "Current Dataset : " + trackable.getName();
            trackable.setUserData(name);
            Log.d("Log", "UserData:Set the following user data "
                    + trackable.getUserData());
        }

        return true;
    }

    @Override
    public boolean doStartTrackers() {
        // Indicate if the trackers were started correctly
        boolean result = true;

        Tracker objectTracker = TrackerManager.getInstance().getTracker(
                ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.start();

        return result;
    }

    @Override
    public boolean doStopTrackers() {
        // Indicate if the trackers were stopped correctly
        boolean result = true;

        Tracker objectTracker = TrackerManager.getInstance().getTracker(
                ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.stop();

        return result;
    }

    @Override
    public boolean doUnloadTrackersData() {
        // Indicate if the trackers were unloaded correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        if (dataset != null && dataset.isActive())
        {
            if (objectTracker.getActiveDataSet().equals(dataset)
                    && !objectTracker.deactivateDataSet(dataset))
            {
                result = false;
            } else if (!objectTracker.destroyDataSet(dataset))
            {
                result = false;
            }

            dataset = null;
        }

        return result;
    }

    @Override
    public boolean doDeinitTrackers() {
        // Indicate if the trackers were deinitialized correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        tManager.deinitTracker(ObjectTracker.getClassType());

        return result;
    }

    @Override
    public void onInitARDone(VuforiaApplicationException exception) {
        if (exception == null) {
            initApplicationAR();

            mRenderer.setActive(true);

            final FrameLayout cameraLayout = (FrameLayout) findViewById(R.id.cameraView);

            // Now add the GL surface view. It is important
            // that the OpenGL ES surface view gets added
            // BEFORE the camera is started and video
            // background is configured.
            cameraLayout.addView(mGlView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));

            try {
                vuforiaAppSession.startAR(CameraDevice.CAMERA_DIRECTION.CAMERA_DIRECTION_DEFAULT);
            } catch (VuforiaApplicationException e) {
                Log.e("Log", e.getString());
            }

            boolean result = CameraDevice.getInstance().setFocusMode(
                    CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO);

            if (!result)
                Log.e("Log", "Unable to enable continuous autofocus");

        } else {
            Log.e("Log", exception.getString());
        }
    }

    // Initializes AR application components.
    private void initApplicationAR() {
        // Create OpenGL ES view:
        int depthSize = 16;
        int stencilSize = 0;
        boolean translucent = Vuforia.requiresAlpha();

        mGlView = new CustomGLView(this);
        ((CustomGLView)mGlView).init(translucent, depthSize, stencilSize);

        mRenderer = new CameraRenderer(this, vuforiaAppSession);
        mGlView.setRenderer(mRenderer);

    }

    boolean mSwitchDatasetAsap = false;

    @Override
    public void onVuforiaUpdate(State state) {
        if (mSwitchDatasetAsap) {
            mSwitchDatasetAsap = false;
            TrackerManager tm = TrackerManager.getInstance();
            ObjectTracker ot = (ObjectTracker) tm.getTracker(ObjectTracker
                    .getClassType());
            if (ot == null || dataset == null
                    || ot.getActiveDataSet() == null) {
                Log.d("Log", "Failed to swap datasets");
                return;
            }

            doUnloadTrackersData();
            doLoadTrackersData();
        }
    }
}
